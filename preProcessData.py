# 12/11/19, Cucco Alessandro, Thesis @ Politecnico di Torino, Italy
# This script computes the activations for the Re3Reloaded model of keras.
# Given a sequence of image data it takes a crop at image i centered in the position of the previous ground truth bounding box and of width and height twice of it. This crop is then 
# passed to a CNN which computes the activations which are then stored into the OUT_PATH directory for easier training of the subsequent layers in the final model (ex an LSTM layer)

import argparse
import logging as log
import signal
import glob
import xmltodict
import threading
import time
import datetime
from utils.utils import *

# wrapper classes
from models.mobileNet.myMobileNet import MyMobileNet
#from mobileNetV2.myMobileNetV2 import MyMobileNetV2
from models.resNet50.myResNet50 import MyResNet50
from models.inceptionV3.myInceptionV3 import MyInceptionV3
from models.templates.myCnn import MyCnn

# constants
IMAGENETVID_PATH = '/home/dale/Alessandro/Thesis/Datasets/ImagenetVID/ILSVRC2017_VID'
OUT_PATH         = '/home/dale/Alessandro/Thesis/Datasets/ImagenetVID/preprocessedData'
CROP_WIDTH  = 224 #TODO change and make it depending on the used cnn
CROP_HEIGHT = 224
DEBUG = False

# graceful termination & timing
terminate = False
startTime = time.time()

# initializes the logger to store debugging messages
def loggerInit():
    log.basicConfig(filename='logs/preprocessData.log', level=log.INFO)
    log.info('Logger initialized')

def preProcessData(wildCardAnnotationPath: str, cnn: MyCnn, semThreads: threading.Semaphore, splitType: str):

    # iterate along all paths matching the wildcard
    for annotationPath in sorted(glob.glob(wildCardAnnotationPath)):

        # wait for semaphore & exit loop if terminate flag
        semThreads.acquire()
        if terminate:
            break

        # setup paths for current sequence
        dataPath = annotationPath.replace('Annotations', 'Data')

        # create thread & start it
        args = [annotationPath, dataPath, cnn, splitType, semThreads]
        t = threading.Thread(target=processData, args=args)
        t.start()

    # wait for all threads to finish their job
    waitThreads()
    if terminate:
        deltaTime = time.time() - startTime
        print('Total time: ', datetime.timedelta(seconds=deltaTime))
        exit(0)


def preProcessTrainData(annotationPath: str, cnn: MyCnn, semThreads: threading.Semaphore):

    # setup wildcard & preProcess data
    wildCardAnnotationPath = os.path.join(annotationPath, 'train', '*', '*')
    preProcessData(wildCardAnnotationPath, cnn, semThreads, 'train')

def preProcessValData(annotationPath: str, cnn: MyCnn, semThreads: threading.Semaphore):

    # setup wildcard & preProcess data
    wildCardAnnotationPath = os.path.join(annotationPath, 'val', '*')
    preProcessData(wildCardAnnotationPath, cnn, semThreads, 'val')

# waits for all started threads if not the main one
def waitThreads():
    main_thread = threading.currentThread()

    for t in threading.enumerate():
        if t is main_thread:
            continue
        t.join()

# converts a bounding box from imagenet vid xml format to 2-point format(xmin, ymin, xmax, ymax)
def to2PointFormat(bBoxAnn: dict):

    xMin = int(bBoxAnn['xmin'])
    yMin = int(bBoxAnn['ymin'])
    xMax = int(bBoxAnn['xmax'])
    yMax = int(bBoxAnn['ymax'])
    bBox = [xMin, yMin, xMax, yMax]
    return bBox

# read an annotation file and returns a dictionary of the bounding boxes of the tracked objects in 2-point format
def readBoxes(path: str):

    boxes = {}

    try:
        with open(path) as fd:

            # parse xml file
            ann = xmltodict.parse(fd.read())
            objects = ann['annotation']['object']

            # transform single object to list since typically we have a list of tracked object per frame
            if not isinstance(objects, list):
                objects = [objects]

            # for each trackid's bounding box transform it into 2-point format(xMin, yMin, xMax, yMax)
            for obj in objects:
                trackId = obj['trackid'] 
                bBoxAnn = obj['bndbox']
                bBox = to2PointFormat(bBoxAnn)
                boxes[trackId] = bBox
    except KeyError:
        log.warning('KeyError caused by: ' + path)

    if DEBUG:
        print("Bounding boxes: ",  boxes)

    return boxes

def processData(annotationPath: str, dataPath: str, cnn: MyCnn, splitType: str, semThread: threading.Semaphore):

    # return if sequence already processed
    seqCode = os.path.basename(os.path.normpath(annotationPath))
    if checkFlagFile(splitType, cnn.getName(), seqCode):
        print('Sequence ' + seqCode + ' already computed, skipping it')
        semThread.release()
        return

    print('Processing: ', annotationPath)

    numLost         = {}
    annotationFiles = sorted(getFiles(annotationPath))
    dataFiles       = sorted(getFiles(dataPath))

    # for all the annotation files with exception of the last one
    for t in range(len(annotationFiles) - 1): # -1 cause we take 2 consecutive timestep datapoints at each timestep

        # define annotation and frame files
        currAnnotationFile = os.path.join(annotationPath, annotationFiles[t])
        nextAnnotationFile = os.path.join(annotationPath, annotationFiles[t+1])
        currDataFile = os.path.join(dataPath, dataFiles[t])
        nextDataFile = os.path.join(dataPath, dataFiles[t+1])

        if DEBUG:
            print('currAnnotationFile: ' + currAnnotationFile)
            print('nextAnnotationFile: ' + nextAnnotationFile)
            print('currDataFile: ' + currDataFile)
            print('nextDataFile: ' + nextDataFile)
        
        # get current and next boxes
        currBoxes = readBoxes(currAnnotationFile)
        nextBoxes = readBoxes(nextAnnotationFile)

        # for all the tracked objects within an annotation
        for trackId, currBox in currBoxes.items():

            # get nLost
            nLost = 0
            if trackId in numLost:
                nLost = numLost[trackId]
            else:
                numLost[trackId] = 0

            # if the object is present in the next timestep crop current image and next one with current box
            if trackId in nextBoxes:

                nextBox = nextBoxes[trackId]

                # crop current image in current timestep's box
                currCrop = cropAndResizeImage(currDataFile, currBox, CROP_WIDTH, CROP_HEIGHT)
                features = cnn.predict(currCrop)
                saveCurrFeatures(features, seqCode, t, trackId, nLost, splitType, cnn.getName())
                #print 'cropped CURR image ' + currDataFile + ' with box ', currBox

                # crop next image in current timestep's box
                nextCrop = cropAndResizeImage(nextDataFile, currBox, CROP_WIDTH, CROP_HEIGHT)
                features = cnn.predict(nextCrop)
                saveNextFeatures(features, seqCode, t, trackId, nLost, splitType, cnn.getName())
                #print 'cropped NEXT image ' + nextDataFile + ' with box ', currBox

                # save nextBox
                saveBox(nextBox, seqCode, t, trackId, nLost, splitType, cnn.getName())

            # if object not present in next timestep update numLost
            else:
                numLost[trackId] += 1

    # save file flag to indicate sequence has been fully processed & signal thread is ending
    saveFlagFile(splitType, cnn.getName(), seqCode)
    semThread.release()

# returns the path to the flag file
def getFlagFilePath(splitType: str, cnnName: str, seqCode: str):
    fileFlagPath = os.path.join(OUT_PATH, splitType, cnnName, seqCode, 'processed')
    return fileFlagPath

# checks if for the split splitType and cnn of name cnnName the sequence seqCode has been successfully processed
def checkFlagFile(splitType: str, cnnName: str, seqCode: str):
    fileFlagPath = getFlagFilePath(splitType, cnnName, seqCode)
    return os.path.isfile(fileFlagPath)

# saves an empty file indicating that for the split splitType, cnn of name cnnName the sequence seqCode has been successfully processed
def saveFlagFile(splitType: str, cnnName: str, seqCode: str):
    fileFlagPath = getFlagFilePath(splitType, cnnName, seqCode)
    open(fileFlagPath, 'w').close()

def saveFeatures(features, seqCode: str, t: int, trackId: str, nLost: int, name: str, splitType: str, cnnName: str):

    # create path to feature
    featPath = os.path.join(OUT_PATH, splitType, cnnName, seqCode, 'trackId_' + trackId, 'subsequence_' + str(nLost), 'timestep_' + str(t), 'features')
    createDir(featPath)

    # save features as numpy array
    featFile = os.path.join(featPath, name)
    np.save(featFile, features)

# saves next timestep features
def saveNextFeatures(features, seqCode, t, trackId, nLost, splitType, cnnName):
    saveFeatures(features, seqCode, t, trackId, nLost, 'next', splitType, cnnName)

# saves current timestep features
def saveCurrFeatures(features, seqCode, t, trackId, nLost, splitType, cnnName):
    saveFeatures(features, seqCode, t, trackId, nLost, 'curr', splitType, cnnName)

def saveBox(box, seqCode, t, trackId, nLost, splitType, cnnName):

    # create dirs to store data
    boxPath  = os.path.join(OUT_PATH, splitType, cnnName, seqCode, 'trackId_' + trackId, 'subsequence_' + str(nLost), 'timestep_' + str(t), 'box')
    createDir(boxPath)

    # save box as text 
    boxFile = os.path.join(boxPath, str(t) + '.txt')
    with open(boxFile, 'w') as f:
        for i in range(len(box) - 1):
            f.write(str(box[i]) + ', ')
        f.write(str(box[len(box) - 1]) + '\n')

# intercepts CTRL-C and sets a flag used for graceful termination of sequences processing
def signalHandler(sig, frame):
    global terminate
    terminate = True
    print('\nReceived INT signal, existing gracefully. Please wait')

# input parsing
def parseInput():

    parser = argparse.ArgumentParser()

    cnnNames = '{}, {}, {}.'.format(MyMobileNet.getName(), MyResNet50.getName(), MyInceptionV3.getName())
    parser.add_argument('-c', '--cnn',     dest='cnnName',  metavar='cnn',      help='cnn to use for feature extraction, currently suported: ' + cnnNames, default=MyMobileNet.getName(), type=str)
    parser.add_argument('-t', '--threads', dest='nThreads', metavar='nThreads', help='number of threads to use when preprocessing sequences',              default=1,                     type=int)
    args = parser.parse_args()
    return args

# returns an instance of the cnn model by name
def getCnn(cnnName):

    if cnnName == MyMobileNet.getName():
        return MyMobileNet()
    elif cnnName == MyResNet50.getName():
        return MyResNet50()
    elif cnnName == MyInceptionV3.getName():
        return MyInceptionV3()
    else:
        print('Unrecognized model name: ' + cnnName + '. Please see the help manual.')
        exit()

def main():

    # define annotation path
    annotationPath = os.path.join(IMAGENETVID_PATH, 'ILSVRC', 'Annotations', 'VID')

    # initialize logger
    loggerInit()

    # parse input
    parseOut = parseInput()
    cnnName  = parseOut.cnnName
    nThreads = parseOut.nThreads

    # create semaphore to manage up to nThreads threads & setup signal handler to gracefully terminate on ctrl-c
    semThreads = threading.Semaphore(nThreads)
    signal.signal(signal.SIGINT, signalHandler)

    # load cnn and compile predict function for multithreading
    cnn : MyCnn = getCnn(cnnName)
    cnn.model._make_predict_function()

    # preprocess data
    print('Preprocessing Imagenet data for ' + cnn.getName() + ' with ' + str(nThreads) + ' threads.')
    preProcessTrainData(annotationPath, cnn, semThreads)
    preProcessValData(annotationPath, cnn, semThreads)

if __name__== "__main__":
  main()
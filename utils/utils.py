#####################################################################
# 12/11/19, Cucco Alessandro, Thesis @ Politecnico di Torino, Italy #
#####################################################################
# this script contains a set of methods used by the other scripts

import os
from PIL import Image
import numpy as np

# returns a list of names containing the names of the 1st level directories given a path
def getSubDirs(path):
    return [name for name in os.listdir(path) if os.path.isdir(os.path.join(path, name))]

# returns the list of filenames contained in a folder specified by path
def getFiles(path):
    return [name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))]

# creates a directory at path if does not already exists
def createDir(path):
    if not os.path.exists(path):
        os.makedirs(path)

# crops and resizes the image
def cropAndResizeImage(imgPath, box, width, height):

    crop    = cropImage(imgPath, box)
    resized = resizeImage(crop, width, height)

    return resized

# crops the image defined at imgPath by using the bounding box bBox expressed in 2 point format (xmin, ymin, xmax, ymax)
def cropImage(imgPath, box):

    img = Image.open(imgPath)

    # get crop size
    bBoxWidth  = box[2] - box[0]
    bBoxHeight = box[3] - box[1]
    cropP1 = (box[0] - bBoxWidth/2.0, box[1] - bBoxHeight/2.0)
    cropP2 = (box[2] + bBoxWidth/2.0, box[3] + bBoxHeight/2.0)

    # crop image
    crop = img.crop((cropP1[0], cropP1[1], cropP2[0], cropP2[1]))
    return crop

# resizes a PIL image by the desired height and width, by keeping the aspect ratio
def resizeImage(img, width, height):

    old_size = img.size 
    ratio = float(width)/max(old_size)
    new_size = tuple([int(x*ratio) for x in old_size])
    resizedImg = img.resize(new_size, Image.ANTIALIAS)
    retCrop = Image.new("RGB", (width, height))
    retCrop.paste(resizedImg, ((width - new_size[0])//2, (height - new_size[1])//2))

    return retCrop

# save box as text
def saveBox(name, box):

    with open(name, 'w') as f:
        for i in range(len(box) - 1):
            f.write(str(box[i]) + ', ')
        f.write(str(box[len(box) - 1]) + '\n')

# read a box as text and return it as a numpy array of floats
def readBox(path):

    with open(path, 'r') as fp:

        # read line of file & remove newline
        line = fp.readline().rstrip()

        # convert from list of string to numpy array of floats
        box = line.split(', ')
        box = np.asarray(box)
        box = box.astype(np.float)
        box = np.expand_dims(box, axis=0)
        #print('box shape: ', box.shape)

    return box
# This repository constains all the code needed to train and evaluate.

## REQUIREMENTS
This is the list of packages/applications needed to run the following code
* keras
* tensorflow-gpu
* numpy
* python3
* CUDA8
* CUDNN7

## TRAINING
The training process is subdivided into 2 phases:
1. preprocess the training datasets, such as Imagenet VID, to obtain both the activations and the ground truth label used later for training the RNN part of the final model
2. training the RNN, which is done by taking the output of the previous point and using it to fit the RNN model

### PREPROCESSING
In order to run the preprocessing, run the following command from the project's root, please note that it will generate a lot of data, so be sure to have at least 300 GB free on your disk for a single model

```bash 
python3 preprocessData.py -c cnnName -t threadCount
```
The arguments are the following:
1. `-h` visualize the help
2. `-c` cnn model for which you want to calculate the activations, please use the previous argument to see the supported models
3. `-t` number of threads to use when preprocessing the datasets, this allows to parallelize the preprocessing operations and execute multiple I/O operations at the same time, thus reducing the total time. Set this parameter to the number of threads you have on your machine to get the best throughput
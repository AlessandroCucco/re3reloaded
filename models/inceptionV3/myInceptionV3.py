#####################################################################
# 12/11/19, Cucco Alessandro, Thesis @ Politecnico di Torino, Italy #
#####################################################################
# this script contains a wrapper clas used to facilitate the usage of InceptionV3

from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_v3 import preprocess_input
from keras.models import Model
from .constants import *
from ..templates.myCnn import MyCnn
import numpy as np

# constants
NAME = 'inceptionV3'

class MyInceptionV3(MyCnn):

    def __init__(self):        

        self.model : Model = MyInceptionV3.getModel()
        self.name  = NAME

    # runs the model on the img, returning the features of the model's outputs
    def predict(self, img):

        # prepare data
        x = np.array(img, dtype=np.float64)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        features = self.model.predict(x)

        return features

    @staticmethod
    def getModel():

        # get base model and freeze weights
        model = InceptionV3(weights='imagenet', include_top=False, input_shape=(WIDTH, HEIGHT, 3))
        for layer in model.layers:
            layer.trainable = False 

        return model

    # returns the name of the cnn
    @staticmethod
    def getName():
        return NAME
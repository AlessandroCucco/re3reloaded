from abc import ABC, abstractmethod

# this class contains the abstract methods that a model needs to implement in order to be used by the training script
class Re3Model(ABC):

    # return the cnn part of the full model
    @staticmethod
    @abstractmethod
    def getCnn():
        pass

    # return the rnn part of the full model
    @staticmethod
    @abstractmethod
    def getRnn():
        pass

    # returns the whole model, consisting of cnn + rnn
    @staticmethod
    @abstractmethod
    def getFullModel():
        pass

    # returns the name of the model
    @staticmethod
    @abstractmethod
    def getName():
        pass
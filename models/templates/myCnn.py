from abc import ABC, abstractmethod

# this class contains the abstract methods that a model needs to implement in order to be used by the preprocessing script
class MyCnn(ABC):

    # runs the model on the img, returning the features of the model's outputs
    @abstractmethod
    def predict(self, img):
        pass

    # returns the cnn model
    @staticmethod
    @abstractmethod
    def getModel():
        pass

    # returns the name of the cnn
    @staticmethod
    @abstractmethod
    def getName():
        pass
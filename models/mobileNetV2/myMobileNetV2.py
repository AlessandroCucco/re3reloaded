#####################################################################
# 12/11/19, Cucco Alessandro, Thesis @ Politecnico di Torino, Italy #
#####################################################################
# this script contains a wrapper clas used to facilitate the usage of MobileNetV2

from keras.applications.mobilenet_v2 import MobileNetV2
from keras.applications.mobilenet_v2 import preprocess_input 
from constants import *
import numpy as np

class MyMobileNetV2:

    def __init__(self):        

        self.model = MyMobileNetV2.getModel()
        self.name = 'mobileNetV2'

    # runs the model on the img, returning the features of the model's outputs
    def predict(self, img):

        # prepare data
        x = np.array(img, dtype=np.float64)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        features = self.model.predict(x)

        return features

    @staticmethod
    def getModel():

        # get base model and freeze weights
        model = MobileNetV2(weights='imagenet', include_top=False, input_shape=(WIDTH, HEIGHT, 3))
        for layer in model.layers:
            layer.trainable = False 

        return model
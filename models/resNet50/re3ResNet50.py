# 12/11/19, Cucco Alessandro, Thesis @ Politecnico di Torino, Italy

from keras.layers import Input, TimeDistributed, Reshape, concatenate, GRU, Dense
from keras.models import Model
from .myResNet50 import MyResNet50
from ..templates.re3Model import Re3Model

# constants 
DEBUG = False
TIMESTEPS = None
RNN_SIZE = 512
DENSE_SIZE = 1024

# layer names
RESHAPE1 = 'reshape1'
RESHAPE2 = 'reshape2'
REGRESSION = 'regression'
CNN1 = 'cnn1'
CNN2 = 'cnn2'

# model name
NAME = 're3ResNet50'

class Re3ResNet50(Re3Model):

    @staticmethod
    def getName():
        return NAME

    # returns the timedistributed version of the model, used when combining it with the RNN during test time
    @staticmethod
    def getCnn():

        # load basic cnn model
        cnn = MyResNet50.getModel()

        # specify input (BatchSize, Timesteps, Width, Height, #Channels)
        inputShape = cnn.get_layer(index=0).output_shape
        input1 = Input(shape=inputShape)

        # apply timedistributed to each base model's output (td = timedistributed)
        tdOut = TimeDistributed(cnn)(input1)
        model = Model(inputs=input1, outputs=tdOut)
        model.summary()

        return model

    # returns the rnn part of re3ResNet50
    @staticmethod
    def getRnn():

        # load full model
        cnnModel = Re3ResNet50.getCnn()

        # get cnn output layer's shape
        cnnOutShape = cnnModel.get_layer(index=-1).output_shape[1:] # time distributed cnn output layer

        # define 2 inputs corresponding to the CNN outputs
        input1 = Input(shape=cnnOutShape) #(TIMESTEPS, 7, 7, 1024)
        input2 = Input(shape=cnnOutShape)

        # reshape & concat to feed to GRU
        x1 = TimeDistributed(Reshape((-1,)), name=RESHAPE1)(input1)
        x2 = TimeDistributed(Reshape((-1,)), name=RESHAPE2)(input2)
        x = concatenate([x1, x2])
        
        # RNN part
        x = TimeDistributed(Dense(DENSE_SIZE))(x)
        x = GRU(RNN_SIZE, return_sequences=True)(x)
        x = GRU(RNN_SIZE, return_sequences=True)(x)
        x = TimeDistributed(Dense(4), name=REGRESSION)(x)

        model = Model(inputs=[input1, input2], outputs=x)
        model.summary()

        return model

    # returns the full model
    @staticmethod
    def getFullModel():

        # load 2 stream cnn
        cnn = Re3ResNet50.getCnn()

        # 2 inputs, each representing a sequence of images
        inputShape = cnn.get_layer(index=0).output_shape[1:]
        input1 = Input(shape=inputShape)
        input2 = Input(shape=inputShape)
        
        # pass inputs through cnn
        x1 = cnn(input1)
        x2 = cnn(input2)

        # load rnn
        rnn = Re3ResNet50.getRnn()
        x = rnn([x1, x2])

        # build final model
        model = Model(inputs=[input1, input2], outputs=x)
        model.summary()

        return model

# just for testing
def main():
    Re3ResNet50.getFullModel()

if __name__ == '__main__':
    main()
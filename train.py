#####################################################################
# 12/11/19, Cucco Alessandro, Thesis @ Politecnico di Torino, Italy #
#####################################################################
# this script contains the code needed to train a model

from trainUtils.trainGenerator import TrainGenerator
from trainUtils.valGenerator import ValGenerator
from keras.callbacks import EarlyStopping, TensorBoard
import numpy as np
from tensorflow import set_random_seed
import argparse
from time import time

# supported re3 models
from models.mobileNet.re3MobileNet import Re3MobileNet
from models.inceptionV3.re3InceptionV3 import Re3InceptionV3
from models.resNet50.re3ResNet50 import Re3ResNet50

# constants
DEBUG = False
RAND_SEED = 9

# training constants
START_BATCH_SIZE = 32
START_TIME_STEPS = 2
NUM_EPOCHS = 1000
NUM_WORKERS = 2
MAX_QUEUE_SIZE = 10

# callbacks constants
EARLY_STOPPING_EPOCHS = 5 # number of epochs after which stop the training if no improvement
TENSORBOARD_DIR = "logs/tensorBoard/{}".format(time()) # where we store tensorboard logs

# debugging purposes only: test that the genrator returns the expected data
def testGenerator():

    trainGen = TrainGenerator(cnnName='mobileNet', batchSize=START_BATCH_SIZE, timeSteps=START_TIME_STEPS)
    trainGen.__getitem__(0)
    exit(0)

# input parsing
def parseInput():

    parser = argparse.ArgumentParser()

    re3Names = '{}, {}, {}.'.format(Re3MobileNet.getName(), Re3ResNet50.getName(), Re3InceptionV3.getName())
    parser.add_argument('-m', '--model', dest='modelName', metavar='re3Model', help='re3 model to use for training, currently suported: ' + re3Names, default=Re3MobileNet.getName(), type=str)
    args = parser.parse_args()
    return args

# returns the rnn model to be used for training
def loadRnn(modelName: str):

    if modelName == Re3MobileNet.getName():
        return Re3MobileNet.getRnn()
    elif modelName == Re3InceptionV3.getName():
        return Re3InceptionV3.getRnn()
    elif modelName == Re3ResNet50.getName():
        return  Re3ResNet50.getRnn()
    else:
        print('Unrecognized model name: ' + modelName + '. Please see the help manual.')
        exit()

def main():

    # fix random seed for reproducibility
    np.random.seed(RAND_SEED)  # numpy
    set_random_seed(RAND_SEED) # tensorflow

    # setup batchSize and TimeSteps
    batchSize = START_BATCH_SIZE
    timeSteps = START_TIME_STEPS

    # parse input & get model's name
    parseOut = parseInput()
    modelName = parseOut.modelName

    # load rnn & compile
    model = loadRnn(modelName)
    model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
    # TODO create loss based on IOU and prefer higher IOU in starting timesteps rather than later

    # setup generators TODO make it work for all supported CNNs
    trainGen = TrainGenerator(cnnName='mobileNet', batchSize=batchSize, timeSteps=timeSteps)
    valGen   = ValGenerator(cnnName='mobileNet', batchSize=batchSize, timeSteps=timeSteps)

    # define callbacks
    earlyStopping = EarlyStopping(patience=EARLY_STOPPING_EPOCHS)
    tensorBoard   = TensorBoard(log_dir=TENSORBOARD_DIR)
    #modelCheckPoint = ModelCheckpoint() # TODO add model saving after n epochs

    # start training
    model.fit_generator(generator=trainGen,
                        epochs=NUM_EPOCHS,
                        validation_data=valGen,
                        shuffle=False,
                        workers=NUM_WORKERS,
                        max_queue_size=MAX_QUEUE_SIZE,
                        callbacks=[earlyStopping, tensorBoard])


if __name__== "__main__":
    main()
from keras.utils import Sequence
import os
import time
import numpy as np
from natsort import natsorted # correctly sort strings containing alphanumerical characters
from utils.utils import getSubDirs, readBox

# constants
DATA_PATH = '/home/dale/Alessandro/Thesis/Datasets/ImagenetVID/preprocessedData'
DEBUG = False

class DataGenerator(Sequence):

    # cnnName: name of the cnn, splitType: type of the data split(train or val)
    def __init__(self, cnnName: str, splitType: str, batchSize: int, timeSteps: int):
        self.batchSize = batchSize
        self.timeSteps = timeSteps
        self.dataPath = os.path.join(DATA_PATH, splitType, cnnName)

        if DEBUG:
            print(self.dataPath)

        # load the names of the examples
        self.pathNames = self.loadPathNames()

    def __len__(self):
        return int(np.ceil(len(self.pathNames) / float(self.batchSize)))

    def __getitem__(self, index: int):

        x1 = [] # final dimensions: (batchSize, timeSteps, sizeOfFeatures)
        x2 = []
        y  = [] # final dimensions: (batchSize, timeSteps, 4)

        # iterate along pathNames starting at index*batchSize
        for p in range(index*self.batchSize, (index + 1)*self.batchSize):

            pathName = self.pathNames[p]
            tempX1, tempX2, tempY = self.loadData(pathName)

            # expand dims to concatenate
            tempX1 = np.expand_dims(tempX1, axis=0)
            tempX2 = np.expand_dims(tempX2, axis=0)
            tempY  = np.expand_dims(tempY,  axis=0)

            # add to list
            x1.append(tempX1)
            x2.append(tempX2)
            y.append(tempY)

        # list of numpy array -> numpy array
        x1 = np.concatenate(x1, axis=0)
        x2 = np.concatenate(x2, axis=0)
        y  = np.concatenate(y, axis=0)

        if DEBUG:
            print('X1 batch: ', x1.shape)
            print('Y batch: ', y.shape)

        return [x1, x2], y

    def loadData(self, path: str):

        x1 = [] # features
        x2 = []
        y  = [] # gt boxes

        #print('loadData path: ', path)
        subSeqPath, timeStepName = os.path.split(path)
        startTimeStep = int(timeStepName.split('_')[1])
        endTimeStep   = startTimeStep + self.timeSteps

        # for all timesteps from current up to endTimeStep
        for t in range(startTimeStep, endTimeStep):

            # setup timestep, features and box dirs
            currTimeStepPath = os.path.join(subSeqPath, 'timestep_' + str(t))
            featPath = os.path.join(currTimeStepPath, 'features')
            boxPath  = os.path.join(currTimeStepPath, 'box', str(t) + '.txt')

            # read features & box
            currFeat, nextFeat = self.readFeatures(featPath)
            box = readBox(boxPath)

            # append to arrays
            x1.append(currFeat)
            x2.append(nextFeat)
            y.append(box)

        # convert from list to numpy array
        x1 = np.concatenate(x1, axis=0)
        x2 = np.concatenate(x2, axis=0)
        y  = np.concatenate(y, axis=0)

        if DEBUG:
            print('\tX1 timestep: ', x1.shape)
            print('\tY timestep: ', y.shape)

        return x1, x2, y

    # reads the features given a path to them
    @staticmethod
    def readFeatures(featPath: str):

        # setup paths to files
        currFeatPath = os.path.join(featPath, 'curr.npy') # TODO make same name as preprocessData with config
        nextFeatPath = os.path.join(featPath, 'next.npy')

        # load the files
        currFeat = np.load(currFeatPath)
        nextFeat = np.load(nextFeatPath)

        if DEBUG:
            print('\t\tShape of single feature: ',  currFeat.shape)

        # return data
        return currFeat, nextFeat


    # loads the name of the starting example
    def loadPathNames(self):

        pathNames = []

        # iterate along all sequences
        for seqName in sorted(getSubDirs(self.dataPath)):

            # set path to sequence and skip if no processed flag file
            seqPath = os.path.join(self.dataPath, seqName)
            processedPath = os.path.join(seqPath, 'processed')
            if not os.path.isfile(processedPath):
                print('Sequence ' + seqName + ' skipped as not fully preprocessed')
                continue

            # iterate along trackIds
            for trackId in sorted(getSubDirs(seqPath)):

                trackPath = os.path.join(seqPath, trackId)

                # iterate along subsequences
                for subSequence in sorted(getSubDirs(trackPath)):

                    subSequencePath = os.path.join(trackPath, subSequence)
                    timeStepCount = len(getSubDirs(subSequencePath))

                    # iterate along timesteps of a sequence
                    for t, timeStepName in enumerate(natsorted(getSubDirs(subSequencePath))):

                        if t > timeStepCount - self.timeSteps:
                            break

                        pathName = os.path.join(subSequencePath, timeStepName)
                        pathNames.append(pathName)

                    '''# iterate along timesteps of a sequence
                    for t in range(subSeqTimeSteps - timeSteps):

                        pathName = os.path.join(subSequencePath, 'timestep_' + str(t))
                        pathNames.append(pathName)'''

        return pathNames
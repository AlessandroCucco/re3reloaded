from keras.utils import Sequence
from trainUtils.dataGenerator import DataGenerator

# wrapper class for creating a generator out of training data
class TrainGenerator(Sequence):

    def __init__(self, cnnName: str, batchSize: int, timeSteps: int):
        self.generator = DataGenerator(cnnName=cnnName, splitType='train', batchSize=batchSize, timeSteps=timeSteps)

    def __len__(self):
        return self.generator.__len__()

    def __getitem__(self, index):
        return self.generator.__getitem__(index=index)